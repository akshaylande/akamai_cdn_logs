package com.vuclip.insights.akamai.transforms;

import com.google.api.services.bigquery.model.TableRow;
import org.apache.beam.sdk.transforms.DoFn;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class GetTableRowDoFn extends DoFn<String, TableRow> {

    private String load_duration;
    private String[] fieldsList;
    private String fieldDelimiter;
    private String prefix;

    public GetTableRowDoFn(String[] fieldsList, String load_duration, String fieldDelimiter, String prefix) {
        this.load_duration = load_duration;
        this.fieldsList = fieldsList;
        this.fieldDelimiter = fieldDelimiter;
        this.prefix=prefix;
    }

    @ProcessElement
    public void processElement(ProcessContext c) throws IOException {

        String[] input = c.element().split(fieldDelimiter);

        TableRow tblRow = new TableRow();
        Date dateObj = null;
        try {
            dateObj = new SimpleDateFormat("dd-MM-yyyy").parse(load_duration);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss zzz");
        String partitionTimeFormated = format.format(dateObj).split(" ")[0];

        tblRow.set("creation_time", partitionTimeFormated + " 00:00:00.000 UTC");

        if (prefix.equalsIgnoreCase("eip"))
        {
            Date dateObj1 = null;
            try {
                dateObj1 = new SimpleDateFormat("yyyy-MM-dd").parse(input[0]);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss zzz");
            String partitionTimeFormated1 = format1.format(dateObj1).split(" ")[0];

            tblRow.set("date", partitionTimeFormated1 + " 00:00:00.000 UTC");

            for (int count = 0; count <= 17; count++) {

                if (count == 3) {
                    tblRow.set(fieldsList[count], "");
                } else {
                    tblRow.set(fieldsList[count], input[count + 1]);
                }
            }
            c.output(tblRow);

        } else if(prefix.equalsIgnoreCase("video")){

            long millisLong = Long.parseLong(input[0].replace(".", ""));
            Date dateObj1  = new Date(millisLong);
            DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss zzz");
            format1.setTimeZone(TimeZone.getTimeZone("UTC"));
            partitionTimeFormated = format1.format(dateObj1);

            tblRow.set("datetime", partitionTimeFormated);

            for (int count = 0; count <fieldsList.length; count++) {
                if (count == 4) {
                    tblRow.set(fieldsList[count], "");
                } else {
                    tblRow.set(fieldsList[count], input[count]);
                }
            }
            c.output(tblRow);
        }
    }
}