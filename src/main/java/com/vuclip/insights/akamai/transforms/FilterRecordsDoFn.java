package com.vuclip.insights.akamai.transforms;

import org.apache.beam.sdk.transforms.DoFn;

import java.io.IOException;
import java.util.ArrayList;

public class FilterRecordsDoFn extends DoFn<String, String> {

    @ProcessElement
    public void processElement(ProcessContext c) throws IOException {


        String str1 = "#Version: 1.0";
        if (c.element().equals(str1) || c.element().contains("#Fields:")) {
            System.out.print("Do Nothing");
        } else {
            c.output(c.element());
        }
    }

}
