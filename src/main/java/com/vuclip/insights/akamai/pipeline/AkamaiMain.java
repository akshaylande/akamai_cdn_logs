package com.vuclip.insights.akamai.pipeline;

import com.google.api.services.bigquery.model.TableFieldSchema;
import com.google.api.services.bigquery.model.TableReference;
import com.google.api.services.bigquery.model.TableRow;
import com.google.api.services.bigquery.model.TableSchema;
import com.vuclip.insights.akamai.model.AkamaiPipelineOptions;
import com.vuclip.insights.akamai.transforms.FilterRecordsDoFn;
import com.vuclip.insights.akamai.transforms.GetTableRowDoFn;
import com.vuclip.insights.akamai.utils.Utils;
import com.vuclip.insights.dataflow.DataflowPipelineOptionsBuilder;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.Compression;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
import org.apache.beam.sdk.io.gcp.bigquery.TableDestination;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.transforms.SerializableFunction;
import org.apache.beam.sdk.transforms.windowing.*;
import org.apache.beam.sdk.values.ValueInSingleWindow;
import org.joda.time.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class AkamaiMain implements Serializable {

    private static Logger LOG = LoggerFactory.getLogger(AkamaiMain.class);

    private static TableSchema schema;

    public TableSchema createAndSetSchema(String flds[], AkamaiPipelineOptions pipelineOptions) {

        List<TableFieldSchema> fieldList = new ArrayList<>();
        fieldList.add(new TableFieldSchema().setName("creation_time").setType("TIMESTAMP"));
        fieldList.add(new TableFieldSchema().setName(pipelineOptions.getTimestampfield()).setType("TIMESTAMP"));

        for (String fld : flds) {
            fieldList.add(new TableFieldSchema().setName(fld).setType("STRING"));
        }
        schema = new TableSchema().setFields(fieldList);
        return schema;
    }

    public void run(String[] args) throws Exception {

        AkamaiPipelineOptions pipelineOptions = (AkamaiPipelineOptions) DataflowPipelineOptionsBuilder.buildPipelineOptions(AkamaiPipelineOptions.class, args);
        pipelineOptions.setJobName("akamai--"+pipelineOptions.getTable().replaceAll("_","-"));

        Pipeline pipeline = Pipeline.create(pipelineOptions);

        String projectId = pipelineOptions.getProject();
        String datasetId = pipelineOptions.getDatasetId();
        String tableName = pipelineOptions.getTable();
        String load_duration = pipelineOptions.getLoadDuration();
        String[] fields = pipelineOptions.getFieldSequenceList().split(",");
        String fieldDelimiter = pipelineOptions.getFieldDelimiter();
        String prefix = pipelineOptions.getPrefix();

        final String SOURCE = pipelineOptions.getGcsBucketPath() + pipelineOptions.getLoadDuration() + "/" + pipelineOptions.getPrefix() + "*";

        System.out.println(SOURCE);

        schema = createAndSetSchema(fields, pipelineOptions);

        pipeline.apply("Read from GZIP Files", TextIO.read().withCompression(Compression.AUTO).from(SOURCE))
                .apply("Filter Header Records", ParDo.of(new FilterRecordsDoFn()))
                .apply("Get TableRow", ParDo.of(new GetTableRowDoFn(fields, load_duration, fieldDelimiter, prefix)))
                .apply("Configure Window", configureWindowOperation())
                .apply("Write To BQ", BigQueryIO.writeTableRows()
                        .withMethod(BigQueryIO.Write.Method.FILE_LOADS)
                        .to(new SerializableFunction<ValueInSingleWindow<TableRow>, TableDestination>() {
                            @Override
                            public TableDestination apply(ValueInSingleWindow<TableRow> input) {
                                return new TableDestination(
                                        new TableReference()
                                                .setProjectId(projectId)
                                                .setDatasetId(datasetId)
                                                .setTableId(tableName.trim().toLowerCase().replaceAll(" ", "_")),
                                        "Table :" + tableName,
                                        new com.google.api.services.bigquery.model.TimePartitioning().setType("DAY").setField("creation_time"));
                            }
                        }).withSchema(schema)
                        .withWriteDisposition(BigQueryIO.Write.WriteDisposition.WRITE_APPEND)
                        .withCreateDisposition(BigQueryIO.Write.CreateDisposition.CREATE_IF_NEEDED));
        pipeline.run().waitUntilFinish();
    }

    private Window<TableRow> configureWindowOperation() {
        return Window.<TableRow>configure()
                .triggering(Repeatedly.forever(AfterFirst.of(
                        AfterPane.elementCountAtLeast(2000),
                        AfterProcessingTime.pastFirstElementInPane().plusDelayOf(
                                Duration.standardSeconds(6)))))
                .discardingFiredPanes();
    }

    public static void main(String[] args) {
        AkamaiMain akamaiDFObj = new AkamaiMain();

        try {
            for (String s : args) {
                System.out.println(s);
            }
            akamaiDFObj.run(args);
        } catch (Exception e) {
            LOG.error("Error - ", e);
        }
    }
}