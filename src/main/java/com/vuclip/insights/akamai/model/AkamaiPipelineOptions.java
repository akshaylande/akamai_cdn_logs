package com.vuclip.insights.akamai.model;

import com.google.api.services.bigquery.model.TableSchema;
import org.apache.beam.runners.dataflow.options.DataflowPipelineOptions;
import org.apache.beam.sdk.options.Default;
import org.apache.beam.sdk.options.Description;
import org.apache.beam.sdk.options.Validation;


public interface AkamaiPipelineOptions extends DataflowPipelineOptions {

    @Description("Project id. Required when running a Dataflow in the cloud. "
            + "See https://cloud.google.com/storage/docs/projects for further details.")
    @Validation.Required
    @Default.InstanceFactory(DefaultProjectFactory.class)
    String getProject();

    void setProject(String value);

    @Description("Config")
    String getConfig();

    void setConfig(String config);

    @Description("Prefix")
    String getPrefix();

    void setPrefix(String prefix);

    @Description("env")
    String getEnv();

    void setEnv(String env);


    @Description("schema")
    TableSchema getSchema();

    void setSchema(TableSchema schema);

    @Description("Table Name")
    String getTable();

    void setTable(String table);

    @Description("Partition Field Name")
    String getTimestampfield();

    void setTimestampfield(String timestampfield);


    @Description("Partition Field Name")
    String getFieldDelimiter();

    void setFieldDelimiter(String fieldDelimiter);

    @Description(" Field Name ")
    int getSkipLoadFieldIndex();

    void setSkipLoadFieldIndex(int skipLoadFieldIndex);

    @Description("Partition Field Name")
    String getDateFormat();

    void setDateFormat(String dateFormat);

    @Description("Partition Field index")
    int getParttionFieldIndex();

    void setParttionFieldIndex(int parttionFieldIndex);

    @Description("Fields Sequence List in coma separated String format")
    String getFieldSequenceList();

    void setFieldSequenceList(String fieldSequenceList);

    @Description("E.g Date ")
    String getLoadDuration();

    void setLoadDuration(String loadDuration);

    String getDatasetId();

    void setDatasetId(String datasetId);

    @Description("Specific GCS bucket")
    String getGcsBucketName();

    void setGcsBucketName(String gcsBucketName);

    @Description("S")
    boolean getPartionFieldExists();

    void setPartionFieldExists(boolean partionFieldExists);

    @Description("gcsBucketPath")
    String getGcsBucketPath();

    void setGcsBucketPath(String gcsBucketPath);

    @Description("pipelineName")
    String getPipelineName();

    void setPipelineName(String pipelineName);

 }
