package com.vuclip.insights.akamai.utils;

import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.vuclip.insights.akamai.model.AkamaiPipelineOptions;

import java.io.IOException;

public class Utils {
    public static Storage getStorage(AkamaiPipelineOptions pipelineOptions) throws IOException {
        //Credentials credentials = GoogleCredentials.fromStream(new FileInputStream(pipelineOptions.getCredentials()));
        return StorageOptions.getDefaultInstance().getService();
        //return StorageOptions.newBuilder().setCredentials(credentials).build().getService();
    }

    public static String getJobName(AkamaiPipelineOptions pipelineOptions) {
        return pipelineOptions.getJobName().replace("_", "-") +

                pipelineOptions.getLoadDuration().replace("_", "-");
    }

    public static String getGCSPath(AkamaiPipelineOptions pipelineOptions) {
        return pipelineOptions.getGcsBucketPath()  + "_"
                +  "_" + pipelineOptions.getLoadDuration();
    }
}
